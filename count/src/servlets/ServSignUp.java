package servlets;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import forms.SignUpForm;
import model.Facade;
import model.User;

@WebServlet("/signup")
public class ServSignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//public static final String CONF_DAO_FACTORY = "daofactory";	

	public static final String ATT_CLIENT = "client";
	public static final String ATT_FORM   = "form";

	public static final String SUCESS_VIEW = "/count.html";
	public static final String FORM_VIEW   = "/index.html";

	@EJB
	private Facade facade;


	//    public void init() throws ServletException {
	//    	this.userDAO = ((DAOFactory) getServletContext().getAttribute(CONF_DAO_FACTORY)).getUtilisateurDao();
	//    	
	//    }

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Display the form */
		this.getServletContext().getRequestDispatcher(FORM_VIEW).forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SignUpForm form = new SignUpForm();
		User user = null;
		Map<String, String> errors;
		try {
			errors = form.validateUser(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			errors = null;
			e.printStackTrace();
		}
		
		if (errors.isEmpty()) {
			user = form.getUser();
			if (facade.getUserByEmail(user.getEmail()) == null) {
				facade.addUser(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword());
				//results = "Succès de la création du client.";
			}
			else {
				response.getWriter().println(user.getJSONNoUser().toJSONString()); // A MODIFIER email
				errors.put("email", "Cette adresse mail existe déjà.");
			}
		}

		
		
		request.setAttribute(ATT_CLIENT, user);
		request.setAttribute(ATT_FORM, form);
//		request.setAttribute(ATT_CLIENT, user);
//		request.setAttribute(ATT_FORM, form);
//		request.setAttribute("caca", "5");
		
		if (form.getErrors().isEmpty()) {
			response.getWriter().println(user.getJSONNoUser().toJSONString()); // A MODIFIER user
			this.getServletContext().getRequestDispatcher(SUCESS_VIEW).forward(request, response);
			//response.sendRedirect(request.getContextPath()+"/index.html");
		} else {

			this.getServletContext().getRequestDispatcher(FORM_VIEW).forward(request, response);
		}
	}
}