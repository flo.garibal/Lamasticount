package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import forms.SignInForm;
import model.User;

@WebServlet("/count-ajax")
public class ServCount extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String ATT_USER         = "utilisateur";
	public static final String ATT_FORM         = "form";
	public static final String ATT_USER_SESSION = "userSession";
	public static final String VIEW              = "/ajax-count-counts.jsp";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Display connexion page */
		response.setContentType("text/html");
		if (request.getSession().getAttribute("userSession") != null) {
			User user = (User) request.getSession().getAttribute("userSession");
		    response.getWriter().println(user.getJSONCounts());
		}
		else {
		    response.getWriter().println("caca");

		}
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
	}
}