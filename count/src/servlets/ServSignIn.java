package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import forms.SignInForm;
import model.Facade;
import model.User;

@WebServlet("/signin")
public class ServSignIn extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String ATT_USER         = "utilisateur";
	public static final String ATT_FORM         = "form";
	public static final String ATT_USER_SESSION = "userSession";
	public static final String VIEW              = "/ajax-count/ajax-count-counts.jsp";
	@EJB
	private Facade facade;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* Display connexion page */
		this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		SignInForm form = new SignInForm();

		User utilisateur = form.connectUser(request);

		HttpSession session = request.getSession();

		if (form.getErrors().isEmpty()) {
			session.setAttribute(ATT_USER_SESSION, utilisateur);
		} else {
			session.setAttribute(ATT_USER_SESSION, null);
		}
		
		if (facade.getUserByEmail(utilisateur.getEmail()) == null) {
			response.getWriter().println(utilisateur.getJSONNoUser().toJSONString());
			
		}else{
			response.getWriter().println(utilisateur.getJSONNoUser().toJSONString()); // A MODIFIER
			//request.setAttribute(ATT_FORM, form);
			//request.setAttribute(ATT_USER, utilisateur);

			//this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
		}
	}
}