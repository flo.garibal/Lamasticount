package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RestrictionFilter implements Filter {

	public static final String CONNEXION_PAGE     = "/index.html";
    public static final String ATT_USER_SESSION = "userSession";

	
	@Override
	public void init(FilterConfig config) throws ServletException {

	}
	
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		
		/* Object cast */
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        /* No filter of static resources */
        String path = request.getRequestURI().substring(request.getContextPath().length());
        if (path.startsWith("/css") || path.startsWith("/js")) {
            chain.doFilter(request, response);
            return;
        }
            
        /* Get tu current session from the request */
        HttpSession session = request.getSession();

        /**
         * if the user object doesn't exist in the current sessions,
         * The user is not connected
         */
        if (session.getAttribute(ATT_USER_SESSION ) == null) {
            /* Redirection to the connexion page */
//            request.getRequestDispatcher(CONNEXION_PAGE).forward(request, response);
        	response.sendRedirect(request.getContextPath() + CONNEXION_PAGE);
        } else {
            /* Display wanted page */
            chain.doFilter(request, response);
        }
	}

	@Override
	public void destroy() {

	}
}
