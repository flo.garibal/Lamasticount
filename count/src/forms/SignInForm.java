package forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

import model.User;

public final class SignInForm {
	private static final String EMAIL_FIELD  = "email";
	private static final String PWD_FIELD   = "password";
	private static final String ALGO_CHIFFREMENT = "SHA-256";

	private String              results;
	private Map<String, String> errors      = new HashMap<String, String>();
	

	public String getResults() {
		return results;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public User connectUser( HttpServletRequest request ) {
		/* Get form field */
		String email = getValueChamp(request, EMAIL_FIELD);
		String passWord = getValueChamp(request, PWD_FIELD);

		User user = new User();

		/* validate email field. */
		try {
			emailValidation(email);
		} catch (Exception e) {
			setError(EMAIL_FIELD, e.getMessage());
		}
		user.setEmail(email);

		/* Validate password field. */
		try {
			passwordValidation(passWord);
		} catch (Exception e) {
			setError(PWD_FIELD, e.getMessage());
		}
		
        ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        passwordEncryptor.setAlgorithm( ALGO_CHIFFREMENT );
        passwordEncryptor.setPlainDigest( false );
        String motDePasseChiffre = passwordEncryptor.encryptPassword( passWord );
		
		user.setPassword(motDePasseChiffre);
		

		/* Init result of global validation. */
		if (errors.isEmpty()) {
			results = "Succès de la connexion.";
		} else {
			results = "Échec de la connexion.";
		}

		return user;
	}

	/**
	 * Validate email.
	 */
	private void emailValidation(String email) throws Exception {
		if (email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
			throw new Exception("Merci de saisir une adresse mail valide.");
		}
	}

	/**
	 * Password validation
	 */
	private void passwordValidation(String password) throws Exception {
		int nbCharMin = 5;
		if (password != null) {
			if (password.length() < nbCharMin) {
				throw new Exception( "Le mot de passe doit contenir au moins " +nbCharMin+ " caractères." );
			}
		} else {
			throw new Exception( "Merci de saisir votre mot de passe." );
		}
	}

	/*
	 * Add an error message to a specific field
	 */
	private void setError(String champ, String message) {
		errors.put(champ, message);
	}

	/*
	 * Return null if the field is empty, if it's not it returns its content 
	 */
	private static String getValueChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp );
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}
}