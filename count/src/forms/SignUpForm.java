package forms;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

import model.Facade;
import model.User;

public final class SignUpForm {
    private static final String FIELD_LASTNAME      = "lastname";
    private static final String FIELD_FIRSTNAME    	= "firstname";
    private static final String FIELD_EMAIL     	= "email";
    private static final String FIELD_PASSWORD     	= "password";
	private static final String ALGO_CHIFFREMENT = "SHA-256";

    private String              results;
    private Map<String, String> errors         		= new HashMap<String, String>();
    private User user;
    
	@EJB
	private Facade facade;
    
    public SignUpForm() {
    }
    

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getResults() {
        return results;
    }

    public Map<String, String> validateUser(HttpServletRequest request) throws Exception {
        String lastName 	= getFieldValue(request, FIELD_LASTNAME);
        String firstName 	= getFieldValue(request, FIELD_FIRSTNAME);
        String email 		= getFieldValue(request, FIELD_EMAIL);
        String password 	= getFieldValue(request, FIELD_PASSWORD);


       	user = new User();

        try {
            lastNameValidation(lastName);
        } catch (Exception e) {
            setError(FIELD_LASTNAME, e.getMessage());
        }
        user.setLastName(lastName);

        try {
            firstNameValidation(firstName);
        } catch (Exception e) {
            setError(FIELD_FIRSTNAME, e.getMessage());
        }
        user.setFirstName(firstName);
        
        try {
            emailValidation(email);
        } catch (Exception e) {
            setError(FIELD_EMAIL, e.getMessage());
        }
        user.setEmail(email);
        
        
        try {
            passwordValidation(password);
        } catch (Exception e) {
            setError(FIELD_PASSWORD, e.getMessage());
        }
        ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
        passwordEncryptor.setAlgorithm( ALGO_CHIFFREMENT );
        passwordEncryptor.setPlainDigest( false );
        String motDePasseChiffre = passwordEncryptor.encryptPassword( password );
		
		user.setPassword(motDePasseChiffre);
		
        if (errors.isEmpty()) {
        	
//        	else {
//        		results = "Échec de la création du client.";
//    			setError(FIELD_EMAIL, "Cette adresse mail existe déjà.");
//        	}
        } 
        else {
            results = "Échec de la création du client.";
        }

        return errors;
    }

    private void lastNameValidation(String lastName) throws Exception {
        if (lastName != null) {
            if (lastName.length() < 2) {
                throw new Exception("Le nom d'utilisateur doit contenir au moins 2 caractères.");
            }
        } else {
            throw new Exception("Merci d'entrer un nom d'utilisateur.");
        }
    }

    private void firstNameValidation(String firstName) throws Exception {
        if (firstName != null && firstName.length() < 2) {
            throw new Exception("Le prénom d'utilisateur doit contenir au moins 2 caractères.");
        }
    }

    private void emailValidation(String email) throws Exception {
        if (email != null && !email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
            throw new Exception("Merci de saisir une adresse mail valide.");
        }
    }

    /*
     * Add an error message to the specific field
     */
    private void setError(String champ, String message) {
        errors.put(champ, message);
    }

    /*
     * Util
     */
    private static String getFieldValue(HttpServletRequest request, String fieldName) {
        String valeur = request.getParameter(fieldName);
        if (valeur == null || valeur.trim().length() == 0) {
            return null;
        } else {
            return valeur;
        }
    }
    
	/**
	 * Password validation
	 */
	private void passwordValidation(String password) throws Exception {
		int nbCharMin = 5;
		if (password != null) {
			if (password.length() < nbCharMin) {
				throw new Exception( "Le mot de passe doit contenir au moins " +nbCharMin+ " caractères." );
			}
		} else {
			throw new Exception( "Merci de saisir votre mot de passe." );
		}
	}
	
	public User getUser() {
		return this.user;
	}
}