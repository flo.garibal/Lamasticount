package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class FriendsInvite extends Invite {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	public FriendsInvite() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
