package model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy =InheritanceType.TABLE_PER_CLASS)
public abstract class Invite {


	private Date date;
	private InviteStatus status;
	@Id
	private int id;
	@OneToOne
	private User sender;
	
	@OneToOne
	private User target;	
	

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public InviteStatus getStatus() {
		return status;
	}
	public void setStatus(InviteStatus status) {
		this.status = status;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public User getTarget() {
		return target;
	}
	public void setTarget(User target) {
		this.target = target;
	}
}
