package model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.json.simple.JSONObject;

@Entity
public class ToDoList {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private StringBuilder content;
	
	@OneToOne
	private Counts count;

	public ToDoList() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StringBuilder getContent() {
		return content;
	}

	public void addContent(String content) {
		this.content.append(content);
	}

	public Counts getCount() {
		return count;
	}

	public void setCount(Counts count) {
		this.count = count;
	}	
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		
		json.put("id", this.id);
		json.put("name", this.name);
		json.put("content", this.content);
		
		return json;		
	}
}
