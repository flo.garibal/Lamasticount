package model;
import java.util.Collection;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Singleton
public class Facade {
	private static Facade facade = null;
	
	@PersistenceContext
	private EntityManager em;
	
	//private Map<Integer, Personne> personnes;
	//private Map<Integer, Adresse> adresses;
	
	public Facade() {
		//this.personnes = new HashMap<Integer, Personne>();
		//this.adresses = new HashMap<Integer, Adresse>();
	}
	
	public void addCount(String name) {
		em.persist(new Counts(name));
		em.flush();
	}
	
	public void addCountByUser(String name, int creatorId) {
		Counts c = new Counts(name);
		User u = getUserById(creatorId);
		
		c.addUser(u);
		u.addCount(c);
		
		em.persist(c);
		em.persist(u);
	}
	
	public void addUser(String firstname, String lastname, String email, String password) {
		em.persist(new User(firstname, lastname, email, password));
		em.flush();
	}
	
	public void addExpense(String name, double price, User paidBy) {
		em.persist(new Expense(name, price, paidBy));
		em.flush();
	}
	
	public void addPaidFor(double value, User user) {
		em.persist(new PaidFor(value, user));
		em.flush();
	}
	
	public void addUserToCount(int userId, int countId) {
		User user = em.find(User.class, userId);
		Counts count = em.find(Counts.class, countId);
		
		em.persist(count);
		em.persist(user);
		
		user.addCount(count);
		count.addUser(user);
		em.flush();
	}
	
	public void addExpenseToCount(int expenseId, int countId) {
		Expense expense = em.find(Expense.class, expenseId);
		Counts count = em.find(Counts.class, countId);
		
		em.persist(count);
		em.persist(expense);
		
		count.addExpense(expense);
		expense.setCount(count);
		em.flush();
	}
	
	public void addFriendToUser(int friendId, int userId) {
		User friend = em.find(User.class, friendId);
		User user = em.find(User.class, userId);
		
		em.persist(friend);
		em.persist(user);
		
		user.addFriend(friend);
		friend.addFriend(user);
		em.flush();
	}
	
	public void addPaidForUserToExpense(int userId, int expenseId) {
		User user = em.find(User.class, userId);
		Expense expense = em.find(Expense.class, expenseId);
		
		em.persist(expense);
		em.persist(user);
		
		expense.addPaidFor(user);
		user.addExpensePaidByMe(expense);
		//user.setExpense(expense);
		em.flush();
	}
	
	
	public Collection<Counts> listCount() {
		//return this.adresses.values();
		TypedQuery<Counts> req = em.createQuery("select c from Counts c", Counts.class);
		return req.getResultList();
	}
	

	
	public Collection<Expense> listExpense() {
		TypedQuery<Expense> req = em.createQuery("select e from Expense e", Expense.class);
		return req.getResultList();
	}
	
	public Collection<PaidFor> listPaidFor() {
		TypedQuery<PaidFor> req = em.createQuery("select pf from PaidFor pf", PaidFor.class);
		return req.getResultList();
	}
	
	public Counts getCountById(int id) {
		TypedQuery<Counts> req = em.createQuery("select c from Counts c where id=" + id, Counts.class);
		Collection<Counts> res = req.getResultList();
		
		if (res.size() > 0) {
			return ((ArrayList<Counts>) res).get(0);
		}
		else {
			return null;
		}
	}
	
	public User getUserById(int id) {
		TypedQuery<User> req = em.createQuery("select u from User u where id=" + id, User.class);
		Collection<User> res = req.getResultList();
		
		if (res.size() > 0) {
			return ((ArrayList<User>) res).get(0);
		}
		else {
			return null;
		}
	}
	
	public User getUserByEmail(String email) {
		TypedQuery<User> req = em.createQuery("select u from User u where email=\'" + email + "\'", User.class);
		Collection<User> res = req.getResultList();
		if (res.size() > 0) {
			return ((ArrayList<User>) res).get(0);
		}
		else {
			return null;
		}
	}
	
	public Expense getExpenseById(int id) {
		TypedQuery<Expense> req = em.createQuery("select e from Expense e where id=" + id, Expense.class);
		Collection<Expense> res = req.getResultList();
		
		if (res.size() > 0) {
			return ((ArrayList<Expense>) res).get(0);
		}
		else {
			return null;
		}
	}

	
	public PaidFor getPaidForById(int id) {
		TypedQuery<PaidFor> req = em.createQuery("select pf from PaidFor pf where id=" + id, PaidFor.class);
		Collection<PaidFor> res = req.getResultList();
		
		if (res.size() > 0) {
			return ((ArrayList<PaidFor>) res).get(0);
		}
		else {
			return null;
		}
	}
	
	
	public static Facade getFacade() {
		if (facade == null) {
			facade = new Facade();
		}
		
		return facade;
	}
	

}
