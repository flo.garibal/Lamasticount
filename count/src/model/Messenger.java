package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Entity
public class Messenger {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
			
	@OneToMany
	private List<Message> messages;

	public Messenger() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void addMessage(String message) {
		Message tmpMess = new Message();
		tmpMess.setContent(message);
		this.messages.add(tmpMess);
	}
	
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	} 
	
	public void setMessagesFromString(List<String> messages) {
		for(String s : messages) {
			this.addMessage(s);
		}
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		
		JSONArray messagesArray = new JSONArray();
		
		for (Message m : messages) {
			JSONObject currMessageJSON = new JSONObject();
			
			currMessageJSON.put("id", m.getId());
			currMessageJSON.put("date", m.getDate());
			currMessageJSON.put("content", m.getContent());
			currMessageJSON.put("senderId", m.getSender().getId());
			currMessageJSON.put("senderFirstName", m.getSender().getFirstName());
		}
		
		json.put("messages", messagesArray);
		
		return json;
	}
	
}
