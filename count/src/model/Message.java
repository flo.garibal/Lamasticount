package model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.json.simple.JSONObject;

@Entity
public class Message {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private Date date;
	private String content;
	
	@OneToOne
	private User sender;
	
	@ManyToOne
	private Messenger messenger;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public Messenger getMessenger() {
		return messenger;
	}

	public void setMessenger(Messenger messenger) {
		this.messenger = messenger;
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		
		json.put("id", this.id);
		json.put("date", this.date);
		json.put("content", this.content);
		json.put("senderId", this.sender.getId());
		json.put("senderFirstName", this.sender.getFirstName());
		
		return json;
	}
}
