package model;

import javax.persistence.OneToOne;

public class CountInvite extends Invite {
	
	@OneToOne
	private Counts count;
	
	public CountInvite() {
		
	}

	public Counts getCount() {
		return count;
	}

	public void setCount(Counts count) {
		this.count = count;
	}
}
