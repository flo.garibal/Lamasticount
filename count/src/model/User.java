package model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;

	@ManyToMany
	private List<User> friends;

	@ManyToMany
	private List<Counts> counts;
	
	@OneToMany(mappedBy="paidBy", fetch=FetchType.EAGER)
	private List<Expense> expensesPaidByMe;
	
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<PaidFor> listPaidFor;
	
	@OneToMany(mappedBy="sender", fetch=FetchType.EAGER)
	private List<Invite> invitesSent;
	
	@OneToMany(mappedBy="target", fetch=FetchType.EAGER)
	private List<Invite> invitesReceived;
	
	public User() {
		this.friends = new ArrayList<User>();
		this.counts = new ArrayList<Counts>();
		this.expensesPaidByMe = new ArrayList<Expense>();
		this.listPaidFor = new ArrayList<PaidFor>();
	}
	
	public User(String firstName, String lastName, String email, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.friends = new ArrayList<User>();
		this.counts = new ArrayList<Counts>();
		this.expensesPaidByMe = new ArrayList<Expense>();
		this.listPaidFor = new ArrayList<PaidFor>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<User> getFriends() {
		return friends;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}
	
	public void addFriend(User friend) {
		this.friends.add(friend);
	}
	

	public List<Counts> getCounts() {
		return counts;
	}

	public void setCounts(List<Counts> counts) {
		this.counts = counts;
	}
	
	public void addCount(Counts count) {
		this.counts.add(count);
	}

	public List<Expense> getExpensesPaidByMe() {
		return expensesPaidByMe;
	}

	public void setExpensesPaidByMe(List<Expense> expensesPaidByMe) {
		this.expensesPaidByMe = expensesPaidByMe;
	}
	
	public void addExpensePaidByMe(Expense expensePaidByMe) {
		this.expensesPaidByMe.add(expensePaidByMe);
	}

	public List<PaidFor> getListPaidFor() {
		return listPaidFor;
	}

	public void setListPaidFor(List<PaidFor> listPaidFor) {
		this.listPaidFor = listPaidFor;
	}

	public void addPaidFor(PaidFor paidFor) {
		this.listPaidFor.add(paidFor);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<Invite> getInvitesSent() {
		return invitesSent;
	}

	public void setInvitesSent(List<Invite> invitesSent) {
		this.invitesSent = invitesSent;
	}
	
	public void addInvitesSent(Invite invitesSent) {
		this.invitesSent.add(invitesSent);
	}

	public List<Invite> getInvitesReceived() {
		return invitesReceived;
	}

	public void addInvitesReceived(Invite invitesReceived) {
		this.invitesReceived.add(invitesReceived);
	}
	
	public void setInvitesReceived(List<Invite> invitesReceived) {
		this.invitesReceived = invitesReceived;
	}

	public static JSONObject getJSONNoUser(){
		JSONObject json = new JSONObject();
			
		json.put("erreur", new Integer(803)); // code erreur fin session
		return json;
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		
		/*
		 * "firstName" 	: "Florian",
		 *	"lastName" 	: "Garibal",
		 *	"email" 	: "flo.garibal@gmail.com"
		 */
		json.put("lastname", this.lastName);
		json.put("firstname", this.firstName);	
		json.put("email", this.email);
	

		/*
		 * "counts" : [
		 *		{...},
		 *		{...}
		 *	]
		 */
		JSONArray countsArray = new JSONArray();
		
		for (Counts c : counts) {
			countsArray.add(c.toJSon());
		}
		
		json.put("counts", countsArray);
		
		
		/*
		 * "friends" : [
		 *		{
		 *			"firstName" : "Florian",
		 *			"lastName" 	: "Garibal",
		 *			"email" 	: "flo.garibal@gmail.com"
		 *		},
		 *		{...}
		 *	]
		 */
		JSONArray friendsArray = new JSONArray();
		
		for (User currFriends : friends) {
			JSONObject currFriendsJSON = new JSONObject();
			
			currFriendsJSON.put("id", currFriends.getId());
			currFriendsJSON.put("firstname", currFriends.getFirstName());
			currFriendsJSON.put("lastname", currFriends.getLastName());
			currFriendsJSON.put("email", currFriends.getEmail());
			
			friendsArray.add(currFriendsJSON);
		}
		
		json.put("friends", friendsArray);
		
		/*
		 * "countsInvite" : [
		 *		{
		 *			"firstName" : "Omar",
		 *			"lastName" 	: "Bennani",
		 *			"count" 	: "Volovent"
		 *		},
		 *		{...},
		 *		{...}
		 *	]
		 */
		
		JSONArray countInvitesArray = new JSONArray();
		for (Invite i : invitesReceived) {
			if (i instanceof CountInvite) {
				JSONObject currCountInviteJson = new JSONObject();
				
				currCountInviteJson.put("id", i.getSender().getId());
				currCountInviteJson.put("firstName", i.getSender().getFirstName());
				currCountInviteJson.put("lastName", i.getSender().getLastName());
				currCountInviteJson.put("count", ((CountInvite) i).getCount().getName());
				
				countInvitesArray.add(currCountInviteJson);
			}
		}
		json.put("countsInvite", countInvitesArray);

		
		/*
		 * "friendsInvite" : [
		 *		{
		 *			"firstName" : "Omar",
		 *			"lastName" 	: "Bennani",
		 *			"email" 	: "omar@maroc.fr"
		 *		},
		 *		{...},
		 *		{...}
		 *	]
		 */
		
		JSONArray friendsInvitesArray = new JSONArray();
		for (Invite i : invitesReceived) {
			if (i instanceof FriendsInvite) {
				JSONObject currFriendInviteJson = new JSONObject();
				
				currFriendInviteJson.put("id", i.getSender().getId());
				currFriendInviteJson.put("firstName", i.getSender().getFirstName());
				currFriendInviteJson.put("lastName", i.getSender().getLastName());
				currFriendInviteJson.put("email", i.getSender().getEmail());
				
				friendsInvitesArray.add(currFriendInviteJson);
			}
		}
		json.put("friendsInviteInvite", friendsInvitesArray);
		
		return json;
	}

	public String getJSONCounts () {
		String json = "";
		
		json +="{"
				+ "\"counts\" : [";
		
		int i = 0;
		for (Counts count : this.counts) {
			if (i > 0) {
				json += ",";
			}
			json +="{"
					+ "\"id\" : \"" + count.getId() + "\","
					+ "\"name\" : \"" + count.getName() + "\","
					+ "\"currency\" : \"" + "euros" + "\","
					+ "\"users\" : [";
			
			int j = 0;
			for (User user : count.getUsers()) {
				if (j > 0) {
					json += ",";
				}
				json += "{"
						+ "\"lastName\" : \"" + user.getLastName() + "\","
						+ "\"firstName\" : \"" + user.getFirstName() + "\","
						+ "\"moneyOwedToOthers\" : [";
						
				double value = 0;
				int k = 0;
				for (User userOther : count.getUsers()) {
					if (userOther.getId() != user.getId()) {
						if (k > 0) {
							json += ",";
						}
						value = 0;
						for (Expense expense : count.getExpenses()) {
							if (expense.getPaidBy().getId() == userOther.getId()) {
								for (User pf : expense.getListPaidFor()) {
									if (pf.getId() == user.getId()) {
										value += expense.getPrice() / expense.getListPaidFor().size();
									}
								}
							}
						}
						
						json += "{"
								+ "\"firstName\" : " + userOther.getFirstName() + "\","
								+ "\"value\" : " + value + "\""
								+ "}";
						
						k++;
					}
				}
						
				json += "]}";
				
				j++;
			}
			
			json += "]";
			
			json += "\"expenses\" : [";
			
			int k = 0;
			for (Expense expense : count.getExpenses()) {
				if (k > 0) {
					json += ",";
				}
				json += "{"
						+ "\"name\" : \"" + expense.getName() + "\","
						+ "\"paidBy\" : \"" + expense.getPaidBy().getFirstName() + "\","
						+ "\"price\" : \"" + expense.getPrice()+ "\","
						+ "\"paidFor\" : [";
						
				int l = 0;
				for (User pf : expense.getListPaidFor()) {
					if (l > 0) {
						json += ",";
					}
					
					json += "{"
							+ "\"value\" : \"" + expense.getPrice() / expense.getListPaidFor().size() + "\","
							+ "\"name\" : \"" + pf.getFirstName() + "\""
							+ "}";
					l++;
				}
				
				json += "]}";
				k++;
			}
			json += "]}";
			
			i++;
		}
		json += "]}";
		
		return json;
	}
	
}
