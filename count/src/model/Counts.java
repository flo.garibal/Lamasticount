package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Entity
public class Counts {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	
	@ManyToMany(mappedBy="counts", fetch=FetchType.EAGER)
	private List<User> users;
	
	@OneToMany(mappedBy="count", fetch=FetchType.EAGER)
	private List<Expense> expenses;
	
	@OneToOne
	private Messenger messenger;

	private Currencies currency;
	
	public Counts() {
		this("");
	}
	
	public Counts(String name) {
		this.name = name;
		users = new ArrayList<User>();
		expenses = new ArrayList<Expense>();
		currency = Currencies.EURO;
		messenger = new Messenger();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}

	public List<Expense> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<Expense> expenses) {
		this.expenses = expenses;
	}
	
	public void addExpense(Expense expense) {
		this.expenses.add(expense);
	}
	
	
	public Messenger getMessenger() {
		return messenger;
	}

	public void setMessenger(Messenger messenger) {
		this.messenger = messenger;
	}

	public Currencies getCurrency() {
		return currency;
	}

	public void setCurrency(Currencies currency) {
		this.currency = currency;
	}

	public JSONObject toJSon() {
		JSONObject json = new JSONObject();
		
		/*
		 *  "id" 		: "0",
		 *  "name" 		: "Lamasticoloc",
		 *  "currency" 	: "euros",
		 */
		json.put("id", this.id);
		json.put("name", this.name);
		json.put("currency", this.currency.toString());
		
		JSONArray usersArray = new JSONArray();

		/*
		 * "users" : [
		 *		{
		 *		"lastName" 			: "Garibal",
		 *		"firstName" 		: "Florian",
		 *		"moneyOwedToOthers" : [
		 *				{...},
		 *				{...}
		 *			]
		 *		},
		 *		{...}
		 * ] 
		 */
		for (User u : this.users) {
			JSONObject currUserJSON = new JSONObject();
			
			/*
			 * "lastName" 	: "Garibal",
			 * "firstName" 	: "Florian"
			 */
			// TODO : Voir pour l'améliorer (cf récupérer tout d'un coup)
			currUserJSON.put("firstname",u.toJSON().get("firstname"));
			currUserJSON.put("lastname",u.toJSON().get("lastname"));
			
			
			Map<User, Double> userOwedAmount = new HashMap<User,Double>();
			
			// CALCUL DE LA SOMME DUE A CHAQUE PERSONNE DU COMPTE
			for (Expense e : this.expenses) {
				if (e.getListPaidFor().contains(u)){
					double moneyOwedBefore = 0;
					if (userOwedAmount.containsKey(e.getPaidBy())) {
						moneyOwedBefore = userOwedAmount.get(e.getPaidBy());
					}
					userOwedAmount.put(e.getPaidBy(),moneyOwedBefore + e.getPrice() / e.getListPaidFor().size());
				}
			}
			
			
			/*
			 *  {
			 *		"id" 		: 1,
			 *		"firstName" : "Paul",
			 *		"name" 		: "Larato",
			 *		"value" 	: "7.5"
			 *	} 
			 */
			JSONArray moneyOwedToOthers = new JSONArray();

			for (Entry<User,Double> currUser : userOwedAmount.entrySet()) {
				JSONObject currOwedUserJSON = new JSONObject();
				
				currOwedUserJSON.put("id", currUser.getKey().getId());
				currOwedUserJSON.put("firstName", currUser.getKey().getFirstName());
				currOwedUserJSON.put("lastName", currUser.getKey().getLastName());
				currOwedUserJSON.put("value", currUser.getValue());
				
				moneyOwedToOthers.add(currOwedUserJSON);
			}
			
			/*
			 * "moneyOwedToOthers" : [
			 * { },
			 * { }
			 * ]			
			 */
			json.put("moneyOwedToOthers", moneyOwedToOthers);
			
			usersArray.add(currUserJSON);
		}
		
		json.put("users", usersArray);		
		
		return json;
	}
	

	
}
