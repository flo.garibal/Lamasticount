										package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Entity
public class Expense {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private Date date;
	private String name;
	private double price;
	
	@ManyToOne
	private Counts count;
	
	@ManyToOne
	private User paidBy;
	
	@OneToMany
	private List<User> listPaidFor;
	
	public Expense() {
		listPaidFor = new ArrayList<User>();
		date = new Date();
	}
	
	public Expense(String name, double price, User paidBy) {
		super();
		this.name = name;
		this.price = price;
		this.paidBy = paidBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public User getPaidBy() {
		return paidBy;
	}

	public void setPaidBy(User paidBy) {
		this.paidBy = paidBy;
	}

	public List<User> getListPaidFor() {
		return listPaidFor;
	}

	public void setListPaidFor(List<User> listPaidFor) {
		this.listPaidFor = listPaidFor;
	}

	public void addPaidFor(User paidFor) {
		this.listPaidFor.add(paidFor);
	}

	public Counts getCount() {
		return count;
	}

	public void setCount(Counts count) {
		this.count = count;
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		
		/* {
		 *  	"date" 		: "31/12/2016",
		 *		"name" 		: "Pizza Lopez",
		 *		"paidBy" 	: "Florian",
		 *		"price" 	: "14.5",
		 *		"paidFor" 	: [
		 *			{...},
		 *			{...}
	     *		]
		 *  }
		 */
		
		json.put("date", this.date);
		json.put("name", this.name);
		json.put("paidById", this.paidBy.getId());
		json.put("paidByFirstName", this.paidBy.getFirstName());
		json.put("price", this.price);
		
		JSONArray paidForArray = new JSONArray();
		
		/*
		 * {
		 * 	"id" 	 	: 1,
		 *	"firstName" : "Florian",
		 *	"value" 	: "4.2"
		 *	}
		 */
		for (User u : count.getUsers()) {
			JSONObject currUserJSON = new JSONObject();
			
			currUserJSON.put("id", u.getId());
			currUserJSON.put("firstName", u.getFirstName());
			
			if (this.listPaidFor.contains(u)) {
				currUserJSON.put("value", this.price / this.listPaidFor.size());
			} else {
				currUserJSON.put("value", 0);
			}
			
			paidForArray.add(currUserJSON);
		}
		
		return json;
	}
	
}
